var express = require('express');
var app = express();
var port = 8888;

app.get('/halo/:name?', (req, res) =>
  res
    .append('Access-Control-Allow-Origin', '*')
    .json(
      { message: `Halo ${req.params.name || 'dunia'}!`}
  )
);

app.get('/health-check', (req, res) =>
  res
    .append('Access-Control-Allow-Origin', '*')
    //.statusCode = 200;
    .json(
      { success: true,
        message: "Alive",
        data: null
      }
  )
);

app.listen(port, () =>
  console.log(`Example app listening on port ${port}!`)
);

module.exports = app;