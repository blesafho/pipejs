var request = require('supertest');
var app = require('../app.js')

describe('GET /health-check', function() {
  it('menampilkan health-check', function(done) {
    // The line below is the core test of our app.
    request(app).get('/health-check').expect('{"success":true,"message":"Alive","data":null}', done);
  });
});

describe('GET /halo', function() {
  it('menampilkan "halo dunia!"', function(done) {
    // The line below is the core test of our app.
    request(app).get('/halo').expect('{"message":"Halo dunia!"}', done);
  });
});

describe('GET /halo/semua', function() {
  it('menampilkan "halo semua!"', function(done) {
    // The line below is the core test of our app.
    request(app).get('/halo/semua').expect('{"message":"Halo semua!"}', done);
  });
});