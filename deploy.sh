#!/bin/sh
echo "code has been deployed to production"

docker login --username $DOCKER_USERNAME --password $DOCKER_PASSWORD

# Build image
docker build -t blesafho/go:nodeapi .

# Push container
docker push blesafho/go:nodeapi

# Create container
#docker container create --name nodejs-web blesafho/go:nodejs

# Start container
#docker container start nodejs-web

# See container logs
#docker container logs -f nodejs-web

# Stop container
#docker container stop nodejs-web

# Remove container
#docker container rm nodejs-web