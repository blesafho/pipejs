FROM node:12-alpine

WORKDIR /usr/src/app

ADD package.json package.json

RUN npm install

ADD app.js app.js

ADD index.js index.js

CMD ["node", "index.js"]